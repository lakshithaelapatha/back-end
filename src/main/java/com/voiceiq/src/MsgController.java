/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voiceiq.src;

import com.voiceiq.util.Message;
import com.voiceiq.util.RestURIConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Lakshitha
 */

@Controller
public class MsgController {

    private static final Logger logger = LoggerFactory.getLogger(MsgController.class);

    @Autowired
    private ServletContext servletContext;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = RestURIConstants.SEND_MSG, method = RequestMethod.POST,produces = "application/json")
    public @ResponseBody
    Message sendMessage(@RequestBody Message msg) {

        if (servletContext.getAttribute("idx") == null) {
            servletContext.setAttribute("idx", 0);
        }
        if (servletContext.getAttribute("msg") == null) {
            servletContext.setAttribute("msg", new LinkedHashSet<>());
        }

        int idx = Integer.parseInt(servletContext.getAttribute("idx").toString());
        servletContext.setAttribute("idx", ++idx);
        msg.setSentDate(new Date());
        msg.setId(idx);
        LinkedHashSet<Message> set=(LinkedHashSet<Message>) servletContext.getAttribute("msg");
        set.add(msg);
        servletContext.setAttribute("msg", set);
        return msg;
    } 
    
    @CrossOrigin(origins = "*")
    @RequestMapping(value = RestURIConstants.GET_ALL_MSG, method = RequestMethod.GET,produces = "application/json")
    public @ResponseBody Set<Message> getAllMessages(){
        return (LinkedHashSet<Message>) servletContext.getAttribute("msg");
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = RestURIConstants.DELETE_MSG, method = RequestMethod.DELETE,produces = "application/json")
    public @ResponseBody Set<Message> deleteMessages(@PathVariable("id") int msgId){
        LinkedHashSet<Message> set=(LinkedHashSet<Message>) servletContext.getAttribute("msg");
        List<Message> list=new ArrayList<>(set);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId()==msgId) {
                list.remove(i);
            }
        }
        servletContext.setAttribute("msg", new LinkedHashSet<>(list));
        return set;
    }
}
