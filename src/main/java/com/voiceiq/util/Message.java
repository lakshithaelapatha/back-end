/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voiceiq.util;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author Lakshitha
 */
public class Message implements Serializable,Comparable<Message> {

    private int id;
    private String message;
    private Date sentDate;
    private String user;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the sentDate
     */
    @JsonSerialize(using=DateSerializer.class)
    public Date getSentDate() {
        return sentDate;
    }

    /**
     * @param sentDate the sentDate to set
     */
    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    /**
     * @return the cookie
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the cookie to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public int compareTo(Message o) {
        return Comparators.DATE.compare(this, o);
    }

    public static class  Comparators{
        public static Comparator<Message> DATE = new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o1.sentDate.compareTo(o2.sentDate);
            }
        };
    }
    
}
