/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voiceiq.util;

/**
 *
 * @author Lakshitha
 */
public class RestURIConstants {
//    public static final String DUMMY_EMP = "/rest/emp/dummy";
//	public static final String GET_EMP = "/rest/emp/{id}";
	public static final String GET_ALL_MSG = "/rest/msg";
	public static final String SEND_MSG = "/rest/msg/send";
	public static final String DELETE_MSG = "/rest/msg/delete/{id}";
}
